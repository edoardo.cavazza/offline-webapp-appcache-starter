# Rainbow cats demo
> Offline working web app using AppCache


Resources:
* [manifest.appcache](http://www.html5rocks.com/en/tutorials/appcache/beginner/)
* [manifest.json](https://developers.google.com/web/updates/2014/11/Support-for-installable-web-apps-with-webapp-manifest-in-chrome-38-for-Android)
* [browserconfig.xml](https://msdn.microsoft.com/en-us/library/dn320426(v=vs.85).aspx)
* [icons](http://realfavicongenerator.net/)