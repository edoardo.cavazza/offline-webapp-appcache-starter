var current = 0;
var images = [];

function hide(images, index) {
    for (var i = 0, len = images.length; i < len; i++) {
        if (i !== index) {
            images[i].style.display = 'none';
        } else {
            images[i].style.display = 'block';
        }
    }
}

function next() {
    current++;
    if (current === images.length) {
        current = 0;
    }
    hide(images, current);
}

document.addEventListener('DOMContentLoaded', function() {
    console.debug('>>> bootstrap');
    if (window.location && window.location.hash && window.location.hash === 'webapp') {
        console.debug('>>> webapp mode');
    }
    images = document.querySelectorAll('ul img');
    hide(images, current);
});
